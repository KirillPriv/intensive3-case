SEPARATOR = '------------------------------------------'


class Menu:
    def __init__(self, name, choices):
        self.name = name
        self.choices = choices
        self.choice_calls = {choice_num: choice_call
                             for choice_num, _, choice_call in self.choices}

    def display(self):
        while True:
            print(SEPARATOR)
            print(self.name)
            for choice_num, choice_name, _ in self.choices:
                print(f'{choice_num} - {choice_name}')

            try:
                choice = int(input('Введите номер пункта меню: '))
            except ValueError:
                choice = None

            if (choice is None) or (choice not in self.choice_calls):
                print('Введите корректный пункт меню')
                continue

            choice_call = self.choice_calls[choice]
            if choice_call is None:
                break
            choice_call()
